import React, { useState } from 'react'
import { Button, Input } from '@material-ui/core';
import { storage, db } from '../config/firebase';
import firebase from 'firebase';
import '../css/ImageUpload.css';


function ImageUploader({ username }) {
  const [caption, setCaption] = useState('');
  const [progress, setProgress] = useState(0);
  const [image, setImage] = useState(null);

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setImage(e.target.files[0])
    } else {
      alert("OOPS Couldnt select image!")
    }
  }

  const handleUpload = () => {
    if (image) {
      if (caption.trim().length > 1) {
        const uploadPostImage = storage.ref(`images/${image.name}`).put(image);
        uploadPostImage.on(
          "state_changed",
          (snapShot) => {
            const progress = Math.round(
              (snapShot.bytesTransferred / snapShot.totalBytes) * 100
            );
            setProgress(progress);
          },
          (error) => {
            alert(error.message);
          },
          () => {
            storage.ref("images")
              .child(image.name)
              .getDownloadURL()
              .then(url => {
                db.collection("posts").add({
                  timeStamp: firebase.firestore.FieldValue.serverTimestamp(),
                  caption: caption,
                  imageUrl: url,
                  userName: username,
                });
                setProgress(0);
                setCaption('');
                setImage(null);
              })
          });
      } else {
        alert("Comment cannot be empty !")
      }
    } else {
      alert("Image is not selected !")
    }

  }

  return (
    <div className="imageupload">
      <progress className="progress" value={progress} max="100" />
      <input type="text" placeholder="Enter a caption" onChange={(e) => { setCaption(e.target.value) }} />

      <input className="file__upload" type="file" onChange={handleChange} />
      <Button
        onClick={handleUpload}
      >
        UPLOAD
      </Button>
    </div>
  )
}

export default ImageUploader
