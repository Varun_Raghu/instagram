import React, { useState, useEffect } from 'react'
import firebase from 'firebase';
import { gsap } from 'gsap';
import { db } from '../config/firebase';
import Avatar from '@material-ui/core/Avatar';
import '../css/Post.css';
import '../css/like/like.css';

function Post({ postId, user, userName, imageUrl, caption }) {
  const [comments, setComments] = useState([]);
  const [likes, setLikes] = useState([]);
  const [comment, setComment] = useState('');
  const [count, setClickCount] = useState(0);
  const [currentPostLike, setcurrentPostLike] = useState(false);

  useEffect(() => {
    let unsubscribe;
    if (postId) {
      unsubscribe = db
        .collection("posts")
        .doc(postId)
        .collection("comments")
        .onSnapshot((snapshot) => {
          setComments(snapshot.docs.map((doc) => doc.data()))
        });
      return (() => {
        unsubscribe();
      })
    }
  }, [postId])

  useEffect(() => {
    let unsubscribe;
    if (postId) {
      unsubscribe = db
        .collection("posts")
        .doc(postId)
        .collection("likes")
        .onSnapshot((snapshot) => {
          setLikes(snapshot.docs.map((doc) => doc.id))
        });
      setcurrentPostLike(likes.includes(user.displayName))
      return (() => {
        unsubscribe();
      })
    }
  }, [likes.length])


  const postComment = (event) => {
    event.preventDefault();
    if (user && user.displayName && comment.trim().length > 0) {
      db.collection("posts").doc(postId).collection("comments").add({
        userName: user.displayName,
        text: comment.trim(),
        timeStamp: firebase.firestore.FieldValue.serverTimestamp(),
      })
    } else {
      alert("Comment cannot be empty!");
    }
    setComment('');
  }

  const addLike = () => {
    db.collection("posts").doc(postId).collection("likes").doc(user.displayName).set({
      timeStamp: firebase.firestore.FieldValue.serverTimestamp(),
    })
  }

  const removeLike = () => {
    db.collection("posts").doc(postId).collection("likes").doc(user.displayName).delete();
  }

  return (
    <div className="post">
      <div className="post__header">
        <Avatar
          alt="Varun"
          src="/static/images/avatar/1.jpg"
          className="header_avatar"
        />
        <h3>{userName}</h3>
      </div>
      <div
        onClick={() => {
          if (count == 1) {
            db.collection("posts").doc(postId).collection("likes").doc(user.displayName).set({
              timeStamp: firebase.firestore.FieldValue.serverTimestamp(),
            })
          } else {
            setClickCount(1)
            setTimeout(() => {
              setClickCount(0)
            }, 1000)
          }
        }}
      >
        <img
          className="post__image"
          src={imageUrl}
          alt="Posts pic"
          height="550px"
        />
      </div>

      <div className="like__section">
        {user && currentPostLike ?
          <div onClick={removeLike} class="like__container">
            <div class="like">❤</div>
            {<div class="like__text">{likes.length} like{likes.length === 1 ? '' : 's'}</div>}
          </div>
          :
          <div onClick={addLike} class="like__container">
            <div class="non-like">♡</div>
            {likes.length > 0 ? <div class="like__text">{likes.length} like{likes.length === 1 ? '' : 's'}</div> : <div class="like__text">Like</div>}
          </div>
        }

      </div>


      <h4 className="post_caption"><strong>{userName}</strong> {caption}</h4>

      {comments.length ? <h4 className="comments_heading">Comments</h4> : <></>}

      <div className="post__comments">
        {comments.map((comment) => {
          return (<p className="comment">
            <b>{comment.userName}</b> {comment.text}
          </p>)
        })}
      </div>


      {user && <form className="post__commentBox">
        <input
          className="post__input"
          type="text"
          placeholder="add comment.."
          value={comment}
          onChange={(e) => setComment(e.target.value)}
        />
        <button
          className="post__button"
          disabled={!comment}
          type="submit"
          onClick={postComment}
        > Post
        </button>
      </form>}
    </div>
  )
}

export default Post
