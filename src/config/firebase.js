
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyCpP1lNRBMvY3stblT2Qe4--GMhfd8TdCM",
    authDomain: "instagram-clone-bcfcd.firebaseapp.com",
    databaseURL: "https://instagram-clone-bcfcd-default-rtdb.firebaseio.com",
    projectId: "instagram-clone-bcfcd",
    storageBucket: "instagram-clone-bcfcd.appspot.com",
    messagingSenderId: "1021061506416",
    appId: "1:1021061506416:web:f0210be0096b9648895bb5",
    measurementId: "G-2LMZL2YMT2"
});

const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();
export { db, auth, storage };

// 1.05 - Is it safe to save  DB cred in front end?