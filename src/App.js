import { useState, useEffect } from 'react';
import Modal from '@material-ui/core/Modal';
import './css/App.css';
import Post from './components/Post';
import { db, auth } from './config/firebase';
import firebase from 'firebase';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Input } from '@material-ui/core';
import ImageUploader from './components/ImageUploader';
import Loader from './components/loader';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

function App() {
  const classes = useStyles();
  const [posts, setPosts] = useState([]);
  const [isLoading, setLoader] = useState(true);
  const [isSignUp, setSignUpModal] = useState(false);
  const [isSignIn, setSignInModal] = useState(false);
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [user, setUser] = useState(null);
  const [modalStyle] = useState(getModalStyle);

  useEffect(() => {
    db.collection('posts').orderBy('timeStamp', 'desc').onSnapshot(snapshot => {
      setLoader(true)
      setPosts(snapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data(),
      })))
      setLoader(false)
    })

  }, []);

  useEffect(() => {
    auth.onAuthStateChanged((authUser) => {
      setUser(authUser);
    })
  }, [user]);

  const handleAuth = (event) => {
    event.preventDefault();
    if (isSignUp) {
      auth.createUserWithEmailAndPassword(email, password)
        .then((authUser) => {
          setSignUpModal(false)
          return authUser.user.updateProfile({
            displayName: userName,
          })
        })
        .catch((error) => { alert(error.message) });
    } else {
      auth.signInWithEmailAndPassword(email, password)
        .then((authUser) => {
          setSignInModal(false)
        })
        .catch((error) => { alert(error.message) });
    }
  }
  if (isLoading) {
    return <Loader />;
  }

  return (
    <div className="app">
      <Modal
        open={isSignUp || isSignIn}
        onClose={() => {
          isSignIn ? setSignInModal(false) : setSignUpModal(false)
        }
        }
      >
        <div style={modalStyle} className={classes.paper}>
          <form className="app_signup">
            <center>
              <img
                className="instagram_logo"
                src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png"
                alt="insta_logo"
              />
            </center>
            {isSignUp && <Input
              type="text"
              placeholder="Username"
              value={userName}
              onChange={(e) => { setUserName(e.target.value) }}
            />}
            <Input
              type="text"
              placeholder="Email"
              value={email}
              onChange={(e) => { setEmail(e.target.value) }}
            />
            <Input
              type="text"
              placeholder="Password"
              value={password}
              onChange={(e) => { setPassword(e.target.value) }}
            />
            <Button type="submit" onClick={handleAuth}>{isSignUp ? 'Sign Up' : 'Sign In'}</Button>
          </form>
        </div>
      </Modal>
      <div className="app_header">
        <img
          className="instagram_logo"
          src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png"
          alt="insta_logo"
        />
        {user ? (
          <Button onClick={() => { auth.signOut() }}>Log out</Button>
        ) : (
            <div>
              <Button onClick={() => { setSignUpModal(true) }}>Sign Up</Button>
              <Button onClick={() => { setSignInModal(true) }}>Sign In</Button>
            </div>
          )
        }
      </div>

      <div className="posts">
        {
          posts.map((post) => {
            return (
              <Post
                user={user}
                key={post.id}
                postId={post.id}
                imageUrl={post.imageUrl}
                userName={post.userName}
                caption={post.caption}
              />
            )
          })
        }
      </div>

      {user ? (
        <ImageUploader username={user.displayName} />
      ) : (
          <h3 className="login__to__continue">Sorry,  login to upload post!</h3>
        )
      }
    </div>
  );
}

export default App;
